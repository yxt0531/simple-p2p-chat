extends Control

enum Type { SERVER, CLIENT }

const MAX_PLAYERS: int = 32

var peer # enet instance

func _ready():
	peer = NetworkedMultiplayerENet.new()
	
# server related callback
func _player_connected(id):
	_append_log("", "User " + str(id) + " joined the server.")
	rpc("_append_log", "", "User " + str(id) + " joined the server.")
	rpc_id(id, "_replace_log", $ChatLog.text)
	rpc_id(id, "_scroll_to_bottom")
	
	print("new player connceted: " + str(id))

func _player_disconnected(id):
	_append_log("", "User " + str(id) + " left the server.")
	rpc("_append_log", "", "User " + str(id) + " left the server.")
	
	print("player disconnceted: " + str(id))
# server callback end

# client related callback
func _connected_ok():
	$MainMenu.visible = false
	_scroll_to_bottom()
	print("successfully connected to server")

func _server_disconnected():
	$MainMenu.visible = true
	$MainMenu.set_info("Server disconnected.")
	print("disconnected from server")

func _connected_fail():
	$MainMenu.set_info("Connection failed, please check your network.")
	peer.close_connection()
	
	print("unable to connect to server")
# client callback end

func _get_time_stamp():
	var hour
	var minute
	var second
	if OS.get_datetime().hour < 10:
		hour = "0" + str(OS.get_datetime().hour)
	else:
		hour = str(OS.get_datetime().hour)
		
	if OS.get_datetime().minute < 10:
		minute = "0" + str(OS.get_datetime().minute)
	else:
		minute = str(OS.get_datetime().minute)
		
	if OS.get_datetime().second < 10:
		second = "0" + str(OS.get_datetime().second)
	else:
		second = str(OS.get_datetime().second)
	
	return hour + ":" + minute + ":" + second

remote func _append_log(user: String, text: String):
	$ChatLog.text = $ChatLog.text + _get_time_stamp() + " " + user + "\n" + text + "\n"
	_scroll_to_bottom()

remote func _replace_log(text: String):
	$ChatLog.text = text

remote func _scroll_to_bottom():
	$ChatLog.cursor_set_line($ChatLog.get_line_count())
	
func _on_Send_button_down():
	_append_log($InputName.text, $InputText.text)
	rpc("_append_log", $InputName.text, $InputText.text)
	
	$InputText.text = ""
	$Send.disabled = true

func _on_InputText_text_changed(new_text):
	if new_text == "":
		$Send.disabled = true
	else:
		$Send.disabled = false

func _on_InputText_text_entered(new_text):
	if not $Send.disabled:
		_on_Send_button_down()

func _on_InputName_text_entered(new_text):
	if not $Send.disabled:
		_on_Send_button_down()

func init_as(type, ip: String, port: int):
	if type == Type.SERVER:
		peer.create_server(port, MAX_PLAYERS)
		get_tree().set_network_peer(peer)
	
		get_tree().connect("network_peer_connected", self, "_player_connected")
		get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
		
		$ChatLog.text = "Server started, listening to port: " + str(port) + "\n"
		print("app started as server")
	elif type == Type.CLIENT:
		peer.create_client(ip, port)
		get_tree().set_network_peer(peer)
	
		get_tree().connect("connected_to_server", self, "_connected_ok")
		get_tree().connect("connection_failed", self, "_connected_fail")
		get_tree().connect("server_disconnected", self, "_server_disconnected")
		
		print("app started as client")
	else:
		print("invalid start type")
