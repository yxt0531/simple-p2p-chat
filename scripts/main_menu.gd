extends ColorRect

func _ready():
	randomize()
	$Dialog/InputPort.text = str(randi() % 65536 + 1024) # start with random port

func _enable_button_connect():
	if Validator.is_ip_address($Dialog/InputIP.text) and Validator.is_number($Dialog/InputPort.text):
		$Dialog/ButtonConnect.disabled = false
	else:
		$Dialog/ButtonConnect.disabled = true
		
func _on_InputPort_text_changed(new_text):
	_enable_button_connect()

func _on_InputIP_text_changed(new_text):
	_enable_button_connect()

func _on_ButtonHost_button_down():
	get_parent().init_as(get_parent().Type.SERVER, $Dialog/InputIP.text, int($Dialog/InputPort.text))
	self.hide()

func _on_ButtonConnect_button_down():
	get_parent().init_as(get_parent().Type.CLIENT, $Dialog/InputIP.text, int($Dialog/InputPort.text))
	set_info("Connecting to server...")
	# self.hide()

func set_info(text: String):
	$LabelInfo.text = text
